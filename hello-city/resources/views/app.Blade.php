<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title', config("app.name"))</title>
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
        
    </head>
    <body class="antialiased">

        @yield('content')

     {{config('projet.slogan')}}
        
        <footer>
            <p>&copy; Copyright {{date('Y')}} &middot; 
                @if( !Route::is('about'))
                    <a href="{{route('about')}}">About us</a> </p>
                @endif
        </footer>
    </body>
</html>
